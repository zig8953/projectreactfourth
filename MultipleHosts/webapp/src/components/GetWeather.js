﻿import React from 'react';


export class GetWeather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://localhost:44340/WeatherForecast")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            return (
                <table border="3">
                    <tr>
                        <th>summary</th>
                        <th>temperatureC</th>
                        <th>temperatureF</th>
                        <th>date</th>
                    </tr>

                    {items.map(item => (
                        <tr>
                            <td>{item.summary}</td>
                            <td>{item.temperatureC}</td>
                            <td>{item.temperatureF}</td>
                            <td>{item.date}</td>
                        </tr>
                    ))}

                </table>

            );
        }
    }
}