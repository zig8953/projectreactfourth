import './App.css';
import { GetWeather } from './components/GetWeather';

function App() {
  return (
    <div className="App">
      <header className="App-header">
              <GetWeather />
      </header>
    </div>
  );
}

export default App; 
